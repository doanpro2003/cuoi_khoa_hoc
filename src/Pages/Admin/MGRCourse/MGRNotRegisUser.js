import React, { useEffect, useState } from "react";
import { Button, Form, Input, message, Select } from "antd";
import { useParams } from "react-router-dom";
import { postNotRegisUser } from "./../../../service/userService";
import { postRegisCourseUser } from "../../../service/courseService";
export default function MGRNotRegisUser() {
  const { Option } = Select;
  const layout = {
    labelCol: {
      span: 8,
    },
    wrapperCol: {
      span: 16,
    },
  };
  const tailLayout = {
    wrapperCol: {
      offset: 8,
      span: 16,
    },
  };
  const formRef = React.useRef(null);
  //   const onGenderChange = (value) => {
  //     switch (value) {
  //       case "male":
  //         formRef.current?.setFieldsValue({
  //           note: "Hi, man!",
  //         });
  //         break;
  //       case "female":
  //         formRef.current?.setFieldsValue({
  //           note: "Hi, lady!",
  //         });
  //         break;
  //       case "other":
  //         formRef.current?.setFieldsValue({
  //           note: "Hi there!",
  //         });
  //         break;
  //       default:
  //         break;
  //     }
  //   };
  let param = useParams();
  const onFinish = (values) => {
    console.log("taikhoanphaihon", values);
    postRegisCourseUser({ maKhoaHoc: param.tk, taiKhoan: values.taiKhoan })
      .then((res) => {
        message.success("ghi danh thành công");
        setTimeout(() => {
          window.location.reload();
        }, 1000);
        console.log(res);
      })
      .catch((err) => {
        message.error("ghi danh thất bại");
        console.log(err);
      });
  };
  const [notRegisUser, setnotRegisUser] = useState([]);
  useEffect(() => {
    postNotRegisUser({
      maKhoaHoc: param.tk,
    })
      .then((res) => {
        // console.log(res);
        setnotRegisUser(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderListNotRegisUser = () => {
    return notRegisUser.map((item) => {
      return <Option value={item.taiKhoan}>{item.hoTen}</Option>;
    });
  };
  return (
    <div>
      {" "}
      <Form
        {...layout}
        ref={formRef}
        name="control-ref"
        onFinish={onFinish}
        style={{
          maxWidth: 600,
        }}
      >
        <Form.Item
          name="taiKhoan"
          label="Tài Khoản chưa ghi danh khóa học"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Select
            placeholder="Nhập vào tài khoản hoặc họ tên người dùng"
            // onChange={onGenderChange}
            allowClear
          >
            {renderListNotRegisUser()}
          </Select>
        </Form.Item>
        <Form.Item
          noStyle
          shouldUpdate={(prevValues, currentValues) =>
            prevValues.gender !== currentValues.gender
          }
        >
          {({ getFieldValue }) =>
            getFieldValue("gender") === "other" ? (
              <Form.Item
                name="customizeGender"
                label="Customize Gender"
                rules={[
                  {
                    required: true,
                  },
                ]}
              >
                <Input />
              </Form.Item>
            ) : null
          }
        </Form.Item>
        <Form.Item {...tailLayout}>
          <Button className="bg-red-500" htmlType="submit">
            ghi danh
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
