import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Button, Form, message, Select } from "antd";
import {
  postNotRegis,
  postRegisCourseUser,
} from "../../../service/courseService";
export default function MGRNotRegisCourse() {
  let param = useParams();
  const [notRegis, setnotRegis] = useState([]);
  const [dataChoose, setdataChoose] = useState([]);

  useEffect(() => {
    postNotRegis(param.tk)
      .then((res) => {
        setnotRegis(res.data);
        // console.log(res);
      })
      .catch((err) => {
        console.log("err", err);
      });
  }, []);
  let dataPostRegisCourseUser = {
    maKhoaHoc: dataChoose.maKhoaHoc,
    taiKhoan: param.tk,
  };
  const { Option } = Select;
  const formItemLayout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 14,
    },
  };
  const onFinish = (values) => {
    console.log("Received values of form: ", values);
    setdataChoose(values);
    postRegisCourseUser({
      maKhoaHoc: values.maKhoaHoc,
      taiKhoan: param.tk,
    })
      .then((res) => {
        message.success("ghi danh thành công");
        setTimeout(() => {
          window.location.reload();
        }, 1000);
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
        message.error("ghi danh thất bại");
      });
  };
  const renderNotRegis = () => {
    return notRegis.map((item) => {
      return (
        <Option name="maKhoaHoc" value={item.maKhoaHoc}>
          {item.tenKhoaHoc}
        </Option>
      );
    });
  };
  return (
    <div>
      <Form
        name="validate_other flex"
        {...formItemLayout}
        onFinish={onFinish}
        initialValues={{
          "input-number": 3,
          "checkbox-group": ["A", "B"],
          rate: 3.5,
        }}
        style={{
          maxWidth: 600,
        }}
      >
        <Form.Item
          name="maKhoaHoc"
          label="khóa học chưa ghi danh"
          hasFeedback
          rules={[
            {
              // required: true,
              message: "Please select your country!",
            },
          ]}
        >
          <Select placeholder="chọn khóa học">{renderNotRegis()}</Select>
        </Form.Item>
        <Form.Item
          wrapperCol={{
            span: 12,
            offset: 6,
          }}
        >
          <Button onClick={() => {}} className="bg-green-500" htmlType="submit">
            Ghi danh
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
