import React from "react";
import { Zoom } from "react-awesome-reveal";
import CountUp from "react-countup";
export default function Countup() {
  return (
    <Zoom triggerOnce duration={4000}>
      <div className="grid grid-cols-1 gap-16  py-20 mx-10 lg:grid-cols-4 lg:gap-52 md:grid-cols-2 md:gap-14 shadow-xl shadow-red-200/50">
        <div className="flex flex-col justify-center items-center">
          <img width={100} src={require("../../img/hocvien.jpg")} alt="" />
          <div className="text-6xl my-5 font-bold text-red-500">
            <CountUp end={2868} duration={2} />
          </div>
          <p className="font-bold ">HỌC VIÊN</p>
        </div>
        <div className="flex flex-col justify-center items-center">
          <img width={100} src={require("../../img/khoahoc.jpg")} alt="" />

          <div className="text-6xl my-5 font-bold text-red-500">
            <CountUp end={135} duration={2} />
          </div>
          <p className="font-bold ">KHÓA HỌC</p>
        </div>
        <div className="flex flex-col justify-center items-center">
          <img width={100} src={require("../../img/giohoc.jpg")} alt="" />

          <div className="text-6xl my-5 font-bold text-red-500">
            <CountUp end={7645} duration={2} />
          </div>
          <p className="font-bold ">GIỜ HỌC</p>
        </div>
        <div className="flex flex-col justify-center items-center">
          <img width={100} src={require("../../img/giangvien.jpg")} alt="" />

          <div className="text-6xl my-5 font-bold text-red-500">
            <CountUp end={205} duration={2} />
          </div>
          <p className="font-bold ">GIẢNG VIÊN</p>
        </div>
      </div>
    </Zoom>
  );
}
