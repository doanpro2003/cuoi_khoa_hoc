import { https } from "./configURL";

// Đăng nhập
export const postLogin = (data) => {
  return https.post("/api/QuanLyNguoiDung/DangNhap", data);
};
// Đăng Ký
export const postRegis = (data) => {
  return https.post("/api/QuanLyNguoiDung/DangKy", data);
};
//lấy danh sách người dùng
export const getListUser = (data) => {
  return https.get(
    "/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP06",
    data
  );
};
// lấy thông tin người dùng
export const postUserInfo = (data) => {
  return https.post("/api/QuanLyNguoiDung/ThongTinTaiKhoan", data);
};
// cập nhật thông tin người dùng
export const putUserInfo = (data) => {
  return https.put("/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung", data);
};
//Quản Lý Người Dùng (Thêm Người Dùng)
export const postAddUser = (data) => {
  return https.post(`/api/QuanLyNguoiDung/ThemNguoiDung`, data);
};
//Quản Lý Người Dùng (xóa người dùng)
export const deleteUser = (TaiKhoan) => {
  return https.delete(`/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${TaiKhoan}`);
};
//Quản Lý Người Dùng (Tìm kiếm người dùng theo tên)
export const getFindUser = (tenNguoiDung) => {
  return https.get(
    `/api/QuanLyNguoiDung/TimKiemNguoiDung?MaNhom=GP06&tuKhoa=${tenNguoiDung}`
  );
};
// Quản Lý người dùng (lấy danh sách người dung chưa ghi danh)
export const postNotRegisUser = (makhoahoc) => {
  return https.post(
    `/api/QuanLyNguoiDung/LayDanhSachNguoiDungChuaGhiDanh`,
    makhoahoc
  );
};
// Quản Lý người dùng (lấy danh sách người dùng chưa xét duyệt)
export const postWaitUser = (makhoahoc) => {
  return https.post(
    `/api/QuanLyNguoiDung/LayDanhSachHocVienChoXetDuyet`,
    makhoahoc
  );
};
// Quản Lý người dùng (lấy danh sách người dùng đã ghi danh vào khóa học đó)
export const postReadyUser = (makhoahoc) => {
  return https.post(
    `/api/QuanLyNguoiDung/LayDanhSachHocVienKhoaHoc`,
    makhoahoc
  );
};
