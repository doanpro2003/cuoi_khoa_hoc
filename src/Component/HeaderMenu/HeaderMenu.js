import React from "react";
import { Desktop, Mobile, Tablet } from "./../../HOC/Responsive";
import HeaderMB from "./HeaderMB";
import HeaderMenuLGMD from "./HeaderMenuLGMD";

export default function HeaderMenu() {
  return (
    <div className="fixed w-screen  shadow-xl shadow-red-200/50 px-5 z-10 bg-white">
      <Desktop>
        <HeaderMenuLGMD />
      </Desktop>
      <Tablet>
        <HeaderMenuLGMD />
      </Tablet>
      <Mobile>
        <HeaderMB />
      </Mobile>
    </div>
  );
}
