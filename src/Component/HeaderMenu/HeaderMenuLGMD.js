
import React, { useEffect, useState } from "react";
import { getportCourse } from "../../service/courseService";
import Navbar from "./Navbar";
export default function HeaderMenuLGMD() {
  const [portCourseArr, setPortCourseArr] = useState([]);
  useEffect(() => {
    getportCourse()
      .then((res) => {
        setPortCourseArr(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div className="text-cyan-900 font-medium py-1">
      <div className="flex justify-between items-center h-full">
        <a className="flex justify-center items-center" href="/">
          <img width={100} src={require("../../img/learning.png")} alt="" />
          <div className="text-left">
            <p
              className=" font-black text-red-600 lg:text-3xl md:text-2xl"
            >
              E-learning
            </p>

          </div>
        </a>

        <ul className="flex space-x-5 lg:space-x-10 md:space-x-2 font-bold">
          <li>
            <a className="text-red-600" href="/">
              Trang chủ
            </a>
          </li>
          <li>
            <a className="text-red-600" href="/listcourse">
              Khóa Học
            </a>
          </li>
          <li className="text-red-600">
            <a href="/findcourse">Tìm Kiếm</a>
          </li>


        </ul>
        <Navbar />
      </div>
    </div>
  );
}
